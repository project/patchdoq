<?php

/**
 * @file
 * patchdoq admin functions.
 *
 * @todo Bulk apply/unapply patches. User checks patches to apply using checkbox/check all functionality.
 * @todo Warning "change site to maintainance mode".
 * @todo Weights for patches?
 */

/**
 * Settings form.
 */
function patchdoq_settings(&$form_state) {
  foreach (module_list() as $module) {
    if ($patches = module_invoke($module, 'patch')) {
      asort($patches);
      $form['names'][] = array(
        '#value' => $module,
      );
      foreach ($patches as $id => $patch) {
        //$form['names']
        $form['names'][$id] = array('#value' => $patch['name']);
        $form['descriptions'][$id] = array('#value' => $patch['description']);

        if (patchdoq_patch_is_applied($patch)) {
          $form['operation'][$id] = array(
            '#value' => l(t('unapply'), 'admin/build/patchdoq/unapply/'. $id),
          );
        }
        else {
          $form['operation'][$id] = array(
            '#value' => l(t('apply'), 'admin/build/patchdoq/apply/'. $id),
          );
        }
      }
    }
  }

  return $form;
}


/**
 * Theme pipe order configuration form.
 *
 * @ingroup themeable
 */
function theme_patchdoq_settings($form) {
  drupal_add_css(drupal_get_path('module', 'patchdoq') .'/patchdoq.css', 'module');

  $header = array(t('Name'), t('Description'), t('Operations'));
  $rows = array();
  foreach (element_children($form['names']) as $id) {
    // Don't take form control structures.
    if (is_array($form['names'][$id])) {
      //$row = array();
      // Module name.
      if (is_numeric($id)) {
        $rows[] = array(array('data' => t('@module module', array('@module' => drupal_render($form['names'][$id]))), 'class' => 'module', 'id' => 'module-'. $form['names'][$id]['#value'], 'colspan' => 3));
      }
      else {
        $rows[] = array(
          array(
            'data' => drupal_render($form['names'][$id]),
            'class' => 'patch',
          ),
          drupal_render($form['descriptions'][$id]),
          drupal_render($form['operation'][$id])
        );
      }
    }
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => t('No patches available.'), 'colspan' => 3));
  }

  $output = theme('table', $header, $rows, array('id' => 'patches'));
  $output .= drupal_render($form);

  return $output;
}

/**
 * Patch apply form.
 */
function patchdoq_apply_form($form_state, $patch) {
  if (patchdoq_patch_apply($patch)) {
    drupal_set_message(t('Patch %patch was applied successfully.', array('%patch' => $patch['name'])));
  }
  else {
    drupal_set_message(t('There was an error while applying patch %patch.', array('%patch' => $patch['name'])), 'error');
  }
  drupal_goto('admin/build/patchdoq');
}

/**
 * Patch unapply form.
 */
function patchdoq_unapply_form($form_state, $patch) {
  if (patchdoq_patch_reverse($patch)) {
    drupal_set_message(t('Patch %patch was unapplied successfully.', array('%patch' => $patch['name'])));
  }
  else {
    drupal_set_message(t('There was an error while unapplying patch %patch.', array('%patch' => $patch['name'])), 'error');
  }
  drupal_goto('admin/build/patchdoq');
}
